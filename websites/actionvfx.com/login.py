browser.get('https://www.actionvfx.com/login')
browser.find_element(By.XPATH, '//*[@id="user_email"]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '//*[@id="password"]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/div[3]/ul/li[3]/a/span')
    incorrectPasswordElement = returnsElementIfExist(By.XPATH, '/html/body/div[3]/div[3]/main/div[3]/div[2]/div[1]/div/form/div[1]')
    return PASSWORD_CORRECT if isPasswordCorrect else ('Invalid Email or password.' in incorrectPasswordElement.get_attribute('innerHTML') if incorrectPasswordElement is not None else False)

isPasswordCorrect = wait.until(isPasswordCorrect)
