browser.get('https://www.mrratsuper.com/login')
browser.find_element(By.NAME, 'username').send_keys(username)
passwordField = browser.find_element(By.NAME, 'password')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    correctPasswordElement = returnsElementIfExist(By.XPATH, '/html/body/header/a[2]/span')
    isPasswordCorrect = correctPasswordElement.get_attribute('innerHTML') == username if correctPasswordElement is not None else False
    return PASSWORD_CORRECT if isPasswordCorrect else browser.find_element(By.XPATH, '//*[@id="login_error1"]').is_displayed()

isPasswordCorrect = wait.until(isPasswordCorrect)
