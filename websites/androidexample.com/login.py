browser.get('https://androidexample.com/login')
browser.find_element(By.XPATH, '/html/body/section[3]/div/div/div/div[2]/form/div/div[1]/input[1]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '/html/body/section[3]/div/div/div/div[2]/form/div/div[1]/input[2]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/section[1]/div/div/div/div[3]/div/ul/li/a')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/section[3]/div/div/div/div[2]/form/div/div[1]/div')

isPasswordCorrect = wait.until(isPasswordCorrect)