browser.get('https://www.agorapolis-altislife.fr/register/')
#browser.find_element(By.CLASS_NAME, 'js-noticeDismiss').click()
browser.find_element(By.CSS_SELECTOR, "input[autocomplete='username']").send_keys(username)
#inputField = browser.find_element(By.CSS_SELECTOR, "input[autocomplete='username']")
#inputField.send_keys(username)
#ActionChains(browser).move_to_element(inputField).click(inputField).send_keys(username).perform()
browser.find_element(By.CSS_SELECTOR, "input[type='email']").send_keys(email)
browser.find_element(By.CSS_SELECTOR, "input[type='password']").send_keys(passphrase)
browser.find_element(By.NAME, 'dob_day').send_keys(birthDate.day)
Select(browser.find_element(By.NAME, 'dob_month')).select_by_value(str(birthDate.month))
browser.find_element(By.NAME, 'dob_year').send_keys(birthDate.year)
browser.find_element(By.NAME, 'email_choice').click()
browser.find_element(By.NAME, 'accept').click()

wait.until(lambda _ : browser.find_element(By.ID, 'js-signUpButton').get_attribute('innerHTML') == "S'inscrire")

# TODO: make a function for reCAPTCHA treatment
browser.switch_to.frame(browser.find_element(By.CSS_SELECTOR, "iframe[title='reCAPTCHA']"))
browser.find_element(By.CLASS_NAME, 'recaptcha-checkbox-border').click()

# TODO: Make web browser visible, if needed
# Would be nice if doable from code, otherwise it's simpler just to have it visible at all times.
# Try interacting with https://stackoverflow.com/a/58045991
# Actually not a priority as not everyone could do that easily it seems.

wait.until(lambda _ : browser.find_elements(By.CLASS_NAME, 'recaptcha-checkbox-checked') != [])

browser.switch_to.default_content()
browser.find_element(By.ID, 'js-signUpButton').click()