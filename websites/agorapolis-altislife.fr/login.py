browser.get('https://www.agorapolis-altislife.fr/login/')
browser.find_element(By.NAME, 'login').send_keys(username)
passwordField = browser.find_element(By.NAME, 'password')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/div/div[1]/nav/div/div[6]/div[1]/a[1]/span[2]')
    elementIncorrectPassword = returnsElementIfExist(By.XPATH, '/html/body/div[2]/div/div[3]/div/div/div/div/div/div[2]')
    return PASSWORD_CORRECT if isPasswordCorrect else (True if elementIncorrectPassword is not None and 'Mot de passe incorrect. Veuillez réessayer.' in elementIncorrectPassword.get_attribute('innerHTML') else False)

isPasswordCorrect = wait.until(isPasswordCorrect)
