browser.get('https://auth0.com/api/auth/login?redirectTo=dashboard')
usernameField = browser.find_element(By.XPATH, '//*[@id="username"]')
usernameField.send_keys(username)
usernameField.send_keys(Keys.ENTER)

def returnsPasswordFieldIfExist(browser):
    passwordField = returnsElementIfExist(By.XPATH, '//*[@id="password"]')
    return passwordField if passwordField is not None else False

wait.until(returnsPasswordFieldIfExist)
passwordField = returnsPasswordFieldIfExist(browser)
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/header/div/div[2]/button[4]/div/img')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '//*[@id="error-element-password"]')

isPasswordCorrect = wait.until(isPasswordCorrect)
