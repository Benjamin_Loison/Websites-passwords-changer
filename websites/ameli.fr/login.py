browser.get('https://assure.ameli.fr/PortailAS/appmanager/PortailAS/assure?connexioncompte_2actionEvt=afficher')
browser.find_element(By.XPATH, '//*[@id="connexioncompte_2nir_as"]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '//*[@id="connexioncompte_2connexion_code"]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/header/div/div/div[2]/div/div/div/div/div/a[2]')
    incorrectPasswordElement = returnsElementIfExist(By.XPATH, '/html/body/div/div/div/div/main/section/div/div/div[2]/div/div[2]/div/div/div/div/div[2]/div/div[2]')
    return PASSWORD_CORRECT if isPasswordCorrect else ('Le numéro de sécurité sociale et le code personnel ne correspondent pas.' in incorrectPasswordElement.get_attribute('innerHTML') if incorrectPasswordElement is not None else False)

isPasswordCorrect = wait.until(isPasswordCorrect)
