browser.get('https://archive.org/account/login')
browser.find_element(By.XPATH, '/html/body/div/main/div/div/div[2]/section[3]/form/label[1]/input').send_keys(username)
passwordField = browser.find_element(By.XPATH, '/html/body/div/main/div/div/div[2]/section[3]/form/label[2]/div/input')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    # For unknown reason By.XPATH returns `Operation is not supported` on a shadow root.
    # If similar shadow root issue happen in the future, make a user-friendly function.
    try:
        isPasswordCorrect = browser.find_element(By.XPATH, '/html/body/app-root').shadow_root.find_elements(By.CSS_SELECTOR, 'header:nth-child(2) > ia-topnav:nth-child(2)')[0].shadow_root.find_elements(By.CSS_SELECTOR, '.topnav > primary-nav:nth-child(1)')[0].shadow_root.find_elements(By.CSS_SELECTOR, '.username') != []
    except:
        isPasswordCorrect = False
    return PASSWORD_CORRECT if isPasswordCorrect else browser.find_element(By.XPATH, '/html/body/div/main/div/div/div[2]/section[3]/form/div[2]/span[2]').is_displayed()

isPasswordCorrect = wait.until(isPasswordCorrect)