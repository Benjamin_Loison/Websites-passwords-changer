browser.get('https://my.arte.tv')
browser.find_element(By.XPATH, '/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/form/div[1]/div[1]/div/input').send_keys(username)
passwordField = browser.find_element(By.XPATH, '/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/form/div[1]/div[2]/div/input')
passwordField.send_keys(password)
browser.execute_script("arguments[0].click();", browser.find_element(By.XPATH, '/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/form/button'))

def isPasswordCorrect(browser):
    isPasswordCorrect = returnsElementIfExist(By.XPATH, '/html/body/div[4]/header/div[2]/div[2]/ul/li[2]/a').is_displayed()
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/form/div[1]/div[3]/p')

isPasswordCorrect = wait.until(isPasswordCorrect)