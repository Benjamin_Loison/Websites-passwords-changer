browser.get('https://arxiv.org/login')
browser.find_element(By.XPATH, '//*[@id="username"]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '//*[@id="password"]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/div[2]/div/b')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/main/content/div/div/form/fieldset/div[1]/div/p')

isPasswordCorrect = wait.until(isPasswordCorrect)