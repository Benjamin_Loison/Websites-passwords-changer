browser.get('https://portail.arbs.com/#/')
fields = browser.find_elements(By.CLASS_NAME, 'v-text-field__slot')
emailField = fields[0].find_elements(By.CSS_SELECTOR, "*")[-1]
emailField.send_keys(username)
passwordField = fields[1].find_elements(By.CSS_SELECTOR, "*")[-1]
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div/div/header/div/div/span[2]')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/div/div/main/div/div/section/div/div[1]/form/div[3]/div/div')

isPasswordCorrect = wait.until(isPasswordCorrect)