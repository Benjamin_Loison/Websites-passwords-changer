browser.get('https://compte.auchan.fr/auth/realms/auchan.fr/protocol/openid-connect/auth?client_id=lark-crest&redirect_uri=https%3A%2F%2Fwww.auchan.fr&response_type=code')
browser.find_element(By.NAME, 'username').send_keys(username)
passwordField = browser.find_element(By.NAME, 'password')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/header/div[1]/div[2]/button/span')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/section/div/span')

isPasswordCorrect = wait.until(isPasswordCorrect)
