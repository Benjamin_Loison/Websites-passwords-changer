browser.get('https://miro.com/login/')
browser.find_element(By.XPATH, '//*[@id="email"]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '//*[@id="password"]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[4]/div[5]/div/div[1]/div[3]/div/div/div[1]/div[1]/div[2]/user-profile/div/div/div/img')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '//*[@id="loginError"]')

isPasswordCorrect = wait.until(isPasswordCorrect)
