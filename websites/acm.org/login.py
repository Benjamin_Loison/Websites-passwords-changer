browser.get('https://myacm.acm.org/Shibboleth.sso/Login?entityID=https://idp.acm.org/idp/shibboleth')
browser.find_element(By.XPATH, '//*[@id="username"]').send_keys(username)
passwordField = browser.find_element(By.XPATH, '//*[@id="pword"]')
passwordField.send_keys(password)
passwordField.send_keys(Keys.ENTER)

def isPasswordCorrect(browser):
    isPasswordCorrect = doesElementExist(By.XPATH, '/html/body/div[2]/div[2]/article/div/div/div/div/div/div/div/article/div[1]/div/h2')
    return PASSWORD_CORRECT if isPasswordCorrect else doesElementExist(By.XPATH, '/html/body/div/main/div/div/div[2]/div/div/div[2]/ul[2]/li[1]/form/fieldset[4]/p')

isPasswordCorrect = wait.until(isPasswordCorrect)