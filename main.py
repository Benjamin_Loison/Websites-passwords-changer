#!/usr/bin/python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException
import subprocess, csv, imaplib, email as email_lib, re, time
from selenium.webdriver import ActionChains
from datetime import datetime
from collections import namedtuple

try:
    __file__
except NameError:
    import os
    path = '/home/benjamin/Desktop/bens_folder/dev/python/projects/Online_authentication_API/'
    os.chdir(path)

def execute(cmd):
    try:
        subprocess.check_output(cmd, shell=True)
    except:
        pass

# Is needed if don't run `browser.quit()` which is a bit the case during development.
execute('killall geckodriver')

# Using headless browser doesn't make sense in case of captchas, or should be able to hide it and make it visible at will (or at least to some extent). Let keep this code at least until I face a captcha.
from selenium.webdriver.firefox.options import Options
options = Options()
#options.add_argument('-headless')

browser = webdriver.Firefox(options=options)
wait = WebDriverWait(browser, 60)

PASSWORD_CORRECT = 2

def returnsElementIfExist(by, value):
    try:
        return browser.find_element(by, value)
    except NoSuchElementException:
        return None

def doesElementExist(by, value):
    return returnsElementIfExist(by, value) is not None

def cleanBrowser(browser):
    browser.delete_all_cookies()
    browser.execute_script('window.localStorage.clear();')
    browser.get('about:blank')

# Rename username to id_ and password to passphrase.
def isPasswordCorrect(website, username, password):
    loc = {
        'username': username,
        'password': password
    }
    exec(open(f'websites/{website}/login.py').read(), None, loc)
    cleanBrowser(browser)
    return loc['isPasswordCorrect']

BirthDate = namedtuple('BirthDate', 'day month year')

def createAccount(website, email, username, firstName, lastName, passphrase, question, answer, birthDate):
    loc = {
        'email': email,
        'username': username,
        'firstName': firstName,
        'lastName': lastName,
        'passphrase': passphrase,
        'question': question,
        'answer': answer,
        'birthDate': birthDate
    }
    exec(open(f'websites/{website}/register.py').read(), None, loc)
    cleanBrowser(browser)
    return loc['username']

##

# Assume no interaction from end-user on algorithm emails before they are treated.
# Using user like emails and not using redirections seem to work fine.
imapUsername = 'benjamin9@altiscraft.fr'
imapPassphrase = 'PASSPHRASE'
imapServer = 'ssl0.ovh.net'

def getDatetimeFromStr(datetimeStr):
    return datetime.strptime(datetimeStr, '%d %b %Y %H:%M:%S %z')

def getDatetimeFromMsg(msg):
    return getDatetimeFromStr(msg['Received'].split('; ')[-1])

def getLatestEmailReceiveDate(imap):
    messages = int(imap.select('INBOX')[1][0])
    if messages == 0:
        return None
    response = imap.fetch(str(messages), '(RFC822)')[1][0][1]
    msg = email_lib.message_from_bytes(response)
    receiveDate = getDatetimeFromMsg(msg)
    return receiveDate

# Pay attention to the regex, not to request any HTTP webpage.
# Note that if someone sends with the correct from, subject and link then even if it's incorrect we are going to click on it.
# We could add some emails verifications or assume the mailbox is already filtering incorrect emails. If we assume that the mailbox already filters emails then `from` and `subject` verification is debatable. As we have access to email source, we should make it safe for anyone, so we should enforce such verifications.
# Make sure to correctly escape using `re.escape()` for instance for constant parts.
def getEmailData(imap, wantedFrom, wantedSubject, regex):
    while True:
        messages = int(imap.select('INBOX')[1][0])
        while messages > 0:
            response = imap.fetch(str(messages), '(RFC822)')[1][0]
            msg = email_lib.message_from_bytes(response[1])
            receiveDate = getDatetimeFromMsg(msg)
            if latestEmailReceiveDate is not None and receiveDate < latestEmailReceiveDate:
                # Let's wait additional emails.
                break
            subject = msg['Subject']
            from_ = msg['From']
            # Can't have equality as starts with `[SPAM] ` for the ACM registration confirmation email on my OVH email.
            if from_ == wantedFrom and subject.endswith(wantedSubject):
                if msg.is_multipart():
                    for part in msg.walk():
                        try:
                            body = part.get_payload(decode=True).decode()
                        except:
                            continue
                        return re.search(regex, body).group()
                else:
                    body = msg.get_payload(decode=True).decode()
                # If don't find a match then the email is different than the known Online authentication one or someone is trying to attack.
                    return re.search(regex, body).group()
            messages -= 1
        time.sleep(1)

imap = imaplib.IMAP4_SSL(imapServer)
imap.login(imapUsername, imapPassphrase)

# Should do that everytime before requesting an email, assuming not parallel requests.
latestEmailReceiveDate = getLatestEmailReceiveDate(imap)
print(f'Receive date: {latestEmailReceiveDate}')

email = 'benjamin9@altiscraft.fr'
username = 'First Last'
firstName = 'First'
lastName = 'Last'
passphrase = 'PASSPHRASE'
question = 'Question'
answer = 'Answer'
birthDate = BirthDate(23, 9, 2000)

website = 'agorapolis-altislife.fr'

##

createAccount(website, email, username, firstName, lastName, passphrase, question, answer, birthDate)

##

print(isPasswordCorrect(website, email, passphrase[::-1]) == PASSWORD_CORRECT)
print(isPasswordCorrect(website, email, passphrase) == PASSWORD_CORRECT)

##

def forgotPassphrase(website, username, passphrase, question, answer):
    loc = {
        'username': username,
        'passphrase': passphrase,
        'question': question,
        'answer': answer
    }
    exec(open(f'websites/{website}/forgot_passphrase.py').read(), None, loc)
    cleanBrowser(browser)

newPassphrase = 'NEW_PASSPHRASE'

forgotPassphrase(website, email, newPassphrase, question, answer)

##

print(isPasswordCorrect(website, email, newPassphrase) == PASSWORD_CORRECT)

##

with open('toTest.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    WEBSITE, USERNAME, PASSWORD = 0, 1, 2
    next(spamreader)
    for row in spamreader:
        website, username, password = row
        print(website, username, password)
        # It seems important for `acm.org` to not revert control assay and the actual one, as current `clearBrowser` doesn't clear other websites that seem to depend on `acm.org`.
        controlAssay = isPasswordCorrect(website, username, password[::-1]) == PASSWORD_CORRECT
        if controlAssay:
            print("Control assay succeeded, that wasn't expected, aborting!")
            exit(1)
        print(f'Test: {isPasswordCorrect(website, username, password) == PASSWORD_CORRECT}', end = "\n\n")

##

imap.close()
imap.logout()

# Closes Firefox and geckodriver.
browser.quit()
